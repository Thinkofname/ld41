
use js::CanvasRenderingContext2D as Context;

const MAX_WIDTH: i32 = 240 - 16;

#[derive(Default)]
pub struct TextBuffer {
    lines: Vec<Line>,
}

#[derive(Clone)]
struct Line {
    length: i32,
    parts: Vec<(&'static str, String, i32)>,
}

impl TextBuffer {
    pub fn new() -> TextBuffer {
        TextBuffer {
            lines: vec![Line {
                length: 0,
                parts: Vec::new(),
            }; 15],
        }
    }

    pub fn push_message(&mut self, ctx: &Context, msg: &str) {
        self.lines.rotate_left(1);
        {
            let line = self.lines.last_mut().unwrap();
            line.parts.clear();
            line.length = 0;
        }

        let mut current_fmt = "#FFFFFF";

        for part in msg.split(" ") {
            let line_len = {
                self.lines.last_mut().unwrap().length
            };

            let part = if part.starts_with('@') {
                current_fmt = "#00FF00";
                &part[1..]
            } else if part.starts_with('~') {
                current_fmt = "#FFFF00";
                &part[1..]
            } else if part.starts_with('#') {
                current_fmt = "#00FFFF";
                &part[1..]
            } else {
                part
            };

            let (part, reset) = if part.ends_with('`') {
                (&part[..part.len() - 1], true)
            } else {
                (part, false)
            };
            let width = ctx.measure_text(part).width();

            if line_len + width > MAX_WIDTH {
                self.lines.rotate_left(1);
                let line = self.lines.last_mut().unwrap();
                line.parts.clear();
                line.length = width + 4;
                line.parts.push((current_fmt, part.to_owned(), 0));
            } else {
                let line = self.lines.last_mut().unwrap();
                line.length += width + 4;
                line.parts.push((current_fmt, part.to_owned(), line_len));
            }
            if reset {
                current_fmt = "#FFFFFF";
            }
        }
    }

    pub fn draw(&mut self, ctx: &Context) {
        for (idx, line) in self.lines.iter().enumerate() {
            let y = idx as i32 * 16;
            for &(fmt, ref part, x) in &line.parts {
                ctx.set_fill_style(fmt);
                ctx.fill_text(&part, 8 + x, 16 + y);
            }
        }
    }
}
const js = require("!string-replace-loader?search=self.wasm_bindgen&replace=self.set_wasm=function(w) { wasm = w; }; self.wasm_bindgen!../out/ld41");

let wasm_path = require("!file-loader?name=[hash].wasm!../out/ld41.notwasm")

let images = {
    dark_room: require("../images/dark_room.png"),
    street: require("../images/street.png"),
    player: require("../images/player.png"),
    bad1: require("../images/bad1.png"),
    arrow: require("../images/arrow.png"),
};

var wasm_exports;
var loaded = false;
let data = fetch(wasm_path);
var __exports = window.wasm_bindgen;
if (WebAssembly.instantiateStreaming) {
    WebAssembly.instantiateStreaming(data, { './ld41': __exports })
        .then(({instance}) => {
            wasm_exports = instance.exports;
            set_wasm(wasm_exports);

            if (loaded) {
                do_start();
            }
        })
        // .catch(e => console.error(e));
} else {
    data
        .then(response => response.arrayBuffer())
        .then(data => WebAssembly.instantiate(data, { './ld41': __exports }))
        .then(({instance}) => {
            wasm_exports = instance.exports;
            set_wasm(wasm_exports);

            if (loaded) {
                do_start();
            }
        })
        .catch(e => console.error(e));
}

window.onload = function() {
    if (wasm_exports) {
        do_start();
    }
    loaded = true;
};

function do_start() {
    Object.keys(images).forEach(key => wasm_bindgen.init_image(key, images[key]));
    wasm_bindgen.start();
}
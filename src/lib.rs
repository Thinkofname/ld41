#![feature(proc_macro, wasm_custom_section, wasm_import_module, slice_rotate)]
extern crate wasm_bindgen;
extern crate specs;
#[macro_use]
extern crate specs_derive;
extern crate itertools;
extern crate fnv;

use wasm_bindgen::prelude::*;
use std::cell::RefCell;
use fnv::*;
use specs::prelude::*;

pub mod js;
use js::*;
use js::CanvasRenderingContext2D as Context;
mod text_buffer;
mod command;
mod screen;
mod entity;

#[derive(Debug)]
pub enum KeyEvent {
    Type(String),
    Backspace,
    Submit,
}

thread_local! {
    // Map of image name to image url
    static IMAGE_MAP: RefCell<FnvHashMap<String, String>> = RefCell::new(FnvHashMap::default());
}

#[wasm_bindgen]
pub fn init_image(name: &str, path: &str) {
    IMAGE_MAP.with(|map| {
        map.borrow_mut().insert(name.to_owned(), path.to_owned());
    })
}

pub type ImageMap = FnvHashMap<String, HTMLImageElement>;
#[derive(Default)]
pub struct DeltaTime(pub f64);
#[derive(Default)]
pub struct ShowArrow(pub bool);

#[wasm_bindgen]
pub fn start() {
    let hack = document.get_element_by_id("font-hack");
    hack.style().set_display("none");

    let mut images: ImageMap = Default::default();
    let mut to_load = FnvHashSet::default();
    IMAGE_MAP.with(|map| {
        for (k, v) in map.borrow().iter() {
            let img = document.create_element_image("img");
            img.set_source(v);
            images.insert(k.clone(), img);
            to_load.insert(k.clone());
        }
    });

    let display_canvas = document.create_element_canvas("canvas");
    let body = document.body();
    body.append_child_canvas(&display_canvas);

    display_canvas.set_width(640);
    display_canvas.set_height(480);

    let d_ctx = display_canvas.get_context_2d("2d");
    d_ctx.set_image_smoothing(false);
    d_ctx.set_fill_style("#000000");

    let canvas = document.create_element_canvas("canvas");
    canvas.set_width(480);
    canvas.set_height(270);
    let ctx = canvas.get_context_2d("2d");
    ctx.set_image_smoothing(false);

    let game_canvas = document.create_element_canvas("canvas");
    game_canvas.set_width(240);
    game_canvas.set_height(270);
    let g_ctx = game_canvas.get_context_2d("2d");
    g_ctx.set_image_smoothing(false);

    ctx.set_font("16px HelvetiPixel");
    g_ctx.set_font("16px HelvetiPixel");
    let tex_buf = text_buffer::TextBuffer::new();

    let (send_evt, events) = ::std::sync::mpsc::channel::<KeyEvent>();

    {
        let send_evt = send_evt.clone();
        let evt = Closure::new(move |e: KeyboardEvent| {
            let key = e.key();
            if key.len() == 1 {
                send_evt.send(KeyEvent::Type(key)).unwrap();
                e.prevent_default();
            }
        });
        document.on_key_press(&evt);
        evt.forget();
    }

    {
        let send_evt = send_evt.clone();
        let evt = Closure::new(move |e: KeyboardEvent| {
            let key = e.key();
            match key.as_str() {
                "Enter" => {
                    e.prevent_default();
                    send_evt.send(KeyEvent::Submit).unwrap()
                },
                "Backspace" => {
                    e.prevent_default();
                    send_evt.send(KeyEvent::Backspace).unwrap()
                },
                _ => {},
            }
        });
        document.on_key_down(&evt);
        evt.forget();
    }

    let mut last_frame = performance.now();
    let mut line_buffer = String::new();
    let mut cursor_state = "";
    let mut cursor_timer = 0.0;
    let mut arrow_timer = 0.0;

    let mut world = World::new();
    world.register::<entity::sprite::Sprite>();
    world.register::<entity::Position>();
    world.register::<entity::Velocity>();
    world.register::<entity::Player>();
    world.register::<entity::MoveTask>();
    world.register::<entity::Health>();
    world.register::<entity::Named>();
    world.register::<entity::AttackTask>();
    world.register::<entity::NPC>();
    world.add_resource(screen::Screen::first());
    world.add_resource(g_ctx);
    world.add_resource(images);
    world.add_resource(DeltaTime(1.0));
    world.add_resource(tex_buf);
    world.add_resource(ShowArrow(true));
    let mut dispatcher = DispatcherBuilder::new()
        .with(entity::TickNPC, "tick_npc", &[])
        .with(entity::ApplyVelocity, "apply_velocity", &[])
        .with(entity::DoMove, "do_move", &[])
        .with(entity::DoAttack, "do_attack", &[])
        .with(screen::TickScreen, "follow_player", &["apply_velocity", "do_move"])
        .with(entity::KillEntities, "kill_entities", &[])
        .with_thread_local(screen::DrawScreen)
        .with_thread_local(entity::sprite::DrawSprites)
        .with_thread_local(entity::DrawHealth)
        .with_thread_local(entity::DrawName)
        .build();

    let player = entity::create_player(world.create_entity());
    let mut game_running = true;

    let draw = move || {
        if !game_running {
            return;
        }
        let start = performance.now();
        let diff = start - last_frame;
        let delta = (diff / (1000.0 / 60.0)).min(2.0);
        last_frame = start;

        if !world.is_alive(player) {
            location.reload();
            game_running = false;
            return;
        }

        world.write_resource::<DeltaTime>().0 = delta;

        let width = body.client_width();
        let height = body.client_height();
        display_canvas.set_width(width);
        display_canvas.set_height(height);

        if !to_load.is_empty() {
            let images = world.write_resource::<ImageMap>();
            to_load.retain(|img| {
                if images.get(img).unwrap().complete() {
                    false
                } else {
                    true
                }
            });
            return;
        }

        cursor_timer += delta;
        if cursor_timer > 50.0 {
            cursor_timer -= 50.0;
            cursor_state = if cursor_state == "" {
                "|"
            } else { "" };
        }

        while let Ok(evt) = events.try_recv() {
            match evt {
                KeyEvent::Type(t) => line_buffer.push_str(&t),
                KeyEvent::Backspace => { line_buffer.pop(); },
                KeyEvent::Submit => {
                    {
                        let mut tex_buf = world.write_resource::<text_buffer::TextBuffer>();
                        tex_buf.push_message(&ctx, &format!("> {}", line_buffer));
                    }
                    let command = command::parse_input(&line_buffer);
                    if let Some(cmd) = command {
                        cmd.do_command(&ctx, &mut world, player);
                    } else {
                        let mut tex_buf = world.write_resource::<text_buffer::TextBuffer>();
                        tex_buf.push_message(&ctx, "I don't know how to do that");
                    }
                    line_buffer.clear();
                },
            }
        }

        ctx.set_fill_style("#ffffff");
        ctx.fill_rect(0, 0, 480, 270);

        // Console/text area
        ctx.set_fill_style("#000000");
        ctx.fill_rect(0, 0, 240, 270);
        ctx.set_fill_style("#ffffff");
        ctx.fill_rect(1, 1, 240 - 2, 270 - 2);
        ctx.set_fill_style("#000000");
        ctx.fill_rect(2, 2, 240 - 4, 270 - 4);
        ctx.set_fill_style("#ffffff");
        ctx.fill_rect(2, 246, 240 - 4, 1);

        ctx.fill_text(&line_buffer, 8, 261);
        if !cursor_state.is_empty() {
            ctx.fill_text(cursor_state, 8 + ctx.measure_text(&line_buffer).width(), 261);
        }
        world.write_resource::<text_buffer::TextBuffer>().draw(&ctx);

        dispatcher.dispatch(&mut world.res);
        world.maintain();

        {
            if world.read_resource::<ShowArrow>().0 {
                arrow_timer += delta * 0.05;
                let images = world.read_resource::<ImageMap>();
                world.read_resource::<Context>().draw_image_scaled(
                    images.get("arrow").unwrap(),
                    240 - 64 - 8 - (arrow_timer.sin() * 8.0) as i32, 30, 64, 64
                );
            }
        }

        ctx.draw_canvas_scaled(&game_canvas, 240, 0, 240, 270);
        // Try and work out the base way to scale the canvas to fit
        d_ctx.set_image_smoothing(false);
        d_ctx.fill_rect(0, 0, width, height);

        let w = (480 * height) / 270;
        let h = (270 * width) / 480;
        let (x, y, w, h) = if width < height || w > width {
            let w = width;
            (0, (height - h) / 2, w, h)
        } else {
            let h = height;
            ((width - w) / 2, 0, w, h)
        };

        d_ctx.draw_canvas_scaled(&canvas, x, y, w, h);
    };
    set_drawer(draw);
}


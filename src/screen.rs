
use js::CanvasRenderingContext2D as Context;
use specs::prelude::*;
use entity;
use entity::sprite;

const SCREENS: &[ScreenDesc] = &[
    ScreenDesc {
        name: "dark_room",
        background: "dark_room",
        width: 400,
        enter_text: "You wake up in a dark room alone. You see an @exit` to the @right.`",
        locks: &[
            &Lock {
                start: 100,
                min_time: 300.0,
                spawns: &[
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Jimmy",
                        offset: (1, 25),
                        health: 20.0,
                    },
                    EntityDesc {
                        delay: 300.0,
                        sprite: &sprite::BAD1,
                        name: "Timmy",
                        offset: (-1, 25),
                        health: 20.0,
                    },
                ],
            }
        ],
        next: "street",
    },
    ScreenDesc {
        name: "street",
        background: "street",
        width: 1000,
        enter_text: "You walk into a street filled with people.`",
        locks: &[
            &Lock {
                start: 50,
                min_time: 120.0,
                spawns: &[
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Sam",
                        offset: (1, 25),
                        health: 20.0,
                    },
                    EntityDesc {
                        delay: 120.0,
                        sprite: &sprite::BAD1,
                        name: "Moe",
                        offset: (1, 25),
                        health: 20.0,
                    },
                ],
            },
            &Lock {
                start: 320,
                min_time: 200.0,
                spawns: &[
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Hope du Angst",
                        offset: (-1, 15),
                        health: 50.0,
                    },
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Fred",
                        offset: (1, 25),
                        health: 20.0,
                    },
                    EntityDesc {
                        delay: 200.0,
                        sprite: &sprite::BAD1,
                        name: "Bryan",
                        offset: (-1, 45),
                        health: 20.0,
                    },
                ],
            },
            &Lock {
                start: 540,
                min_time: 200.0,
                spawns: &[
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Ben",
                        offset: (1, 25),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Ryan",
                        offset: (1, 25),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 200.0,
                        sprite: &sprite::BAD1,
                        name: "Steven",
                        offset: (1, 25),
                        health: 30.0,
                    },
                ],
            },
            &Lock {
                start: 700,
                min_time: 500.0,
                spawns: &[
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Imma",
                        offset: (1, 05),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Outta",
                        offset: (1, 25),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 0.0,
                        sprite: &sprite::BAD1,
                        name: "Time",
                        offset: (1, 45),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 200.0,
                        sprite: &sprite::BAD1,
                        name: "Thanks",
                        offset: (-1, 25),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 300.0,
                        sprite: &sprite::BAD1,
                        name: "For",
                        offset: (-1, 35),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 400.0,
                        sprite: &sprite::BAD1,
                        name: "Playing",
                        offset: (1, 25),
                        health: 30.0,
                    },
                    EntityDesc {
                        delay: 500.0,
                        sprite: &sprite::BAD1,
                        name: "Though",
                        offset: (-1, 35),
                        health: 30.0,
                    },
                ],
            },
        ],
        next: "street",
    },
];

pub struct ScreenDesc {
    name: &'static str,
    background: &'static str,
    pub width: i32,
    enter_text: &'static str,
    locks: &'static [&'static Lock],
    next: &'static str,
}

pub struct Lock {
    start: i32,
    spawns: &'static [EntityDesc],
    min_time: f64,
}

pub struct EntityDesc {
    delay: f64,
    sprite: &'static sprite::SpriteInfo,
    name: &'static str,
    offset: (i32, i32),
    health: f64,
}

pub struct Screen {
    pub desc: &'static ScreenDesc,
    pub offset: f64,
    has_entered: bool,
    remaining_locks: Vec<&'static Lock>,
    is_locked: bool,
    waiting_entities: Vec<Entity>,
    lock_time: f64,
    spawn_time: f64,
    spawn_idx: usize,
}

impl Default for Screen {
    fn default() -> Screen {
        Screen::first()
    }
}

impl Screen {
    pub fn first() -> Screen {
        Screen::from_desc(&SCREENS[0])
    }

    fn from_desc(desc: &'static ScreenDesc) -> Screen {
        Screen {
            desc: desc,
            offset: 0.0,
            has_entered: false,
            remaining_locks: desc.locks.to_owned(),
            is_locked: false,
            waiting_entities: Vec::new(),
            lock_time: 0.0,
            spawn_time: 0.0,
            spawn_idx: 0,
        }
    }
}

pub struct DrawScreen;

impl <'a> System<'a> for DrawScreen {
    type SystemData = (
        Write<'a, Screen>,
        Read<'a, Context>,
        Read<'a, ::ImageMap>,
        Write<'a, ::text_buffer::TextBuffer>,
    );
    fn run(&mut self, (mut screen, ctx, images, mut tex_buf): Self::SystemData) {
        if !screen.has_entered {
            screen.has_entered = true;
            tex_buf.push_message(&*ctx, screen.desc.enter_text);
        }
        if let Some(background) = images.get(screen.desc.background) {
            ctx.draw_image_part(background, screen.offset as i32, 0, 240, 270, 0, 0, 240, 270);
        }
    }
}


pub struct TickScreen;

impl <'a> System<'a> for TickScreen {
    type SystemData = (
        Entities<'a>,
        Read<'a, ::DeltaTime>,
        Read<'a, LazyUpdate>,
        Write<'a, Screen>,
        Write<'a, ::ShowArrow>,
        WriteStorage<'a, entity::Position>,
        ReadStorage<'a, entity::Player>,
        WriteStorage<'a, entity::MoveTask>,
        WriteStorage<'a, sprite::Sprite>,
    );
    fn run(&mut self, (ents, delta, updater, mut screen, mut arrow, mut pos, player, mut task, mut spr): Self::SystemData) {
        let delta = delta.0;
        let screen: &mut Screen = &mut *screen;
        if !screen.is_locked {
            if let Some(lock) = screen.remaining_locks.first() {
                if screen.offset >= lock.start as f64 {
                    screen.offset = lock.start as f64;
                    screen.is_locked = true;
                    arrow.0 = false;
                    screen.lock_time = 0.0;
                    screen.spawn_idx = 0;
                    screen.spawn_time = 0.0;
                }
            }
        }
        if screen.is_locked {
            if let Some(lock) = screen.remaining_locks.first().cloned() {
                screen.lock_time += delta;
                screen.spawn_time += delta;
                while let Some(desc) = lock.spawns.get(screen.spawn_idx) {
                    if screen.spawn_time < desc.delay {
                        break;
                    }
                    screen.spawn_time -= desc.delay;
                    let e = updater.create_entity(&ents)
                        .with(entity::Position {
                            x: lock.start as f32 + 120.0 + desc.offset.0 as f32 * 300.0,
                            y: desc.offset.1 as f32,
                            z: 0.0,
                        })
                        .with(entity::sprite::Sprite::new(desc.sprite))
                        .with(entity::Health {
                            health: desc.health,
                            max_health: desc.health,
                        })
                        .with(entity::Named {
                            name: desc.name.into(),
                        })
                        .with(entity::MoveTask {
                            x: lock.start as f32 + 120.0 + desc.offset.0 as f32 * 100.0,
                            y: desc.offset.1 as f32,
                            z: 0.0,
                            time: 200.0,
                        })
                        .with(entity::NPC::new())
                        .build();
                        screen.spawn_idx += 1;
                    screen.waiting_entities.push(e);
                }
                screen.waiting_entities.retain(|e| ents.is_alive(*e));
                if screen.waiting_entities.is_empty() && screen.lock_time >= lock.min_time {
                    screen.remaining_locks.remove(0);
                    arrow.0 = true;
                    screen.is_locked = false;
                }
            } else {
                screen.is_locked = false;
            }
        } else {
            for (e, pos, _, spr) in (&*ents, &mut pos, player.mask(), &mut spr).join() {
                // TODO: Screen locking
                let target_offset = pos.x as f64 - 70.0;
                if target_offset > screen.offset {
                    screen.offset += delta * 0.5;
                }
                if screen.offset >= screen.desc.width as f64 - 240.0 {
                    screen.offset = screen.desc.width as f64 - 240.0;
                }
                if pos.x >= screen.desc.width as f32 - 40.0 {
                    // Change screen
                    let desc = SCREENS.iter()
                        .find(|v| v.name == screen.desc.next)
                        .unwrap();
                    *screen = Screen::from_desc(desc);
                    pos.x = 34.0;
                    pos.y = 25.0;
                    pos.z = 0.0;
                    task.remove(e);
                    spr.set_state(sprite::SpriteState::Idle);
                }
            }
        }
    }
}
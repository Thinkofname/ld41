
use specs::prelude::*;
use specs::storage::NullStorage;
use screen::Screen;
use js::CanvasRenderingContext2D as Context;

pub mod sprite;

pub fn create_player(e: EntityBuilder) -> Entity {
    e
        .with(Position {
            x: 34.0,
            y: 25.0,
            z: 0.0,
        })
        .with(Player)
        .with(sprite::Sprite::new(&sprite::PLAYER))
        .with(Health {
            health: 250.0,
            max_health: 250.0,
        })
        .build()
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Named {
    pub name: String,
}


#[derive(Component)]
#[storage(VecStorage)]
pub struct Health {
    pub health: f64,
    pub max_health: f64,
}

#[derive(Debug)]
pub enum Direction {
    Left,
    Right,
    Up,
    Down,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct MoveTask {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub time: f64,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct AttackTask {
    pub target: Entity,
    pub time: f64,
    pub offset: f32,
}

#[derive(Component, Clone)]
#[storage(VecStorage)]
pub struct Position {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct Player;

#[derive(Component)]
#[storage(VecStorage)]
pub struct NPC {
    action_timer: f64,
}

impl NPC {
    pub fn new() -> NPC {
        NPC {
            action_timer: 45.0,
        }
    }
}
pub struct TickNPC;

impl <'a> System<'a> for TickNPC {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        Read<'a, ::DeltaTime>,
        Read<'a, Screen>,
        WriteStorage<'a, NPC>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Player>,
        ReadStorage<'a, MoveTask>,
        ReadStorage<'a, AttackTask>,
    );
    fn run(&mut self, (ents, updater, delta, screen, mut npc, pos_t, player, mt, at): Self::SystemData) {
        let delta = delta.0;
        for (e, npc, pos, _, _) in (&*ents, &mut npc, &pos_t, !mt.mask(), !at.mask()).join() {
            npc.action_timer -= delta;
            if npc.action_timer <= 0.0 {
                npc.action_timer = 50.0 + ::js::random() * 250.0;
                if ::js::random() < 0.6 {
                    let x = screen.offset as f32 + ::js::random() as f32 * 240.0;
                    let y = ::js::random() as f32 * 50.0;
                    updater.insert(e, MoveTask {
                        x: x,
                        y: y,
                        z: 0.0,
                        time: (y - pos.y).hypot(x - pos.x) as f64,
                    });
                } else {
                    let target = (&*ents, &pos_t, player.mask())
                        .join()
                        .next().unwrap();
                    updater.insert(e, AttackTask {
                        target: target.0,
                        offset: if target.1.x < pos.x {
                            30.0
                        } else {
                            -30.0
                        },
                        time: (target.1.y - pos.y).hypot(target.1.x - pos.x) as f64,
                    });
                }
            }
        }
    }
}

pub struct ApplyVelocity;

impl <'a> System<'a> for ApplyVelocity {
    type SystemData = (
        Entities<'a>,
        Read<'a, ::DeltaTime>,
        Read<'a, LazyUpdate>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
    );
    fn run(&mut self, (ents, delta, updater, mut pos, mut vel): Self::SystemData) {
        let delta = delta.0;
        for (e, pos, vel) in (&*ents, &mut pos, &mut vel).join() {
            pos.x += vel.x * delta as f32;
            pos.y += vel.y * delta as f32;
            pos.z += vel.z * delta as f32;

            let orig = vel.x;
            vel.x -= vel.x * delta as f32 * 0.05;
            if vel.x.signum() != orig.signum() {
                vel.x = 0.0;
            }
            let orig = vel.y;
            vel.y -= vel.y * delta as f32 * 0.05;
            if vel.x.signum() != orig.signum() {
                vel.y = 0.0;
            }

            vel.z -= delta as f32 * 0.1;
            if pos.z <= 0.0 {
                vel.z = 0.0;
                pos.z = 0.0;
            }

            if vel.x*vel.x + vel.y*vel.y + vel.z*vel.z < 0.01 {
                updater.remove::<Velocity>(e);
            }

        }
    }
}

pub struct KillEntities;

impl <'a> System<'a> for KillEntities {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Health>,
    );
    fn run(&mut self, (ents, health): Self::SystemData) {
        for (e, health) in (&*ents, &health).join() {
            if health.health <= 0.0 {
                let _ = ents.delete(e);
            }
        }
    }
}

pub struct DoMove;

impl <'a> System<'a> for DoMove {
    type SystemData = (
        Entities<'a>,
        Read<'a, Screen>,
        Read<'a, ::DeltaTime>,
        Read<'a, LazyUpdate>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, MoveTask>,
        WriteStorage<'a, sprite::Sprite>,
        ReadStorage<'a, Velocity>,
        ReadStorage<'a, Player>,
    );
    fn run(&mut self, (ents, screen, delta, updater, mut pos, mut task, mut spr, vel, player): Self::SystemData) {
        let delta = delta.0;
        let offset = screen.offset as f32;
        for (e, pos, task, spr) in (&*ents, &mut pos, &mut task, &mut spr).join() {
            if vel.get(e).is_some() {
                updater.remove::<MoveTask>(e);
                continue;
            }
            if task.x < pos.x {
                spr.facing = -1;
            } else {
                spr.facing = 1;
            }
            pos.x += ((task.x - pos.x) / task.time as f32) * delta as f32;
            pos.y += ((task.y - pos.y) / task.time as f32) * delta as f32;
            pos.z += ((task.z - pos.z) / task.time as f32) * delta as f32;
            task.time -= delta;
            spr.set_state(sprite::SpriteState::Walk);

            if player.get(e).is_some() {
                if pos.x < offset {
                    pos.x = offset;
                } else if pos.x > offset + 245.0 {
                    pos.x = offset + 245.0;
                }
                if pos.y < 0.0 {
                    pos.y = 0.0;
                } else if pos.y > 50.0 {
                    pos.y = 50.0;
                }
            }

            if task.time <= 0.0 {
                updater.remove::<MoveTask>(e);
                spr.set_state(sprite::SpriteState::Idle);
            }
        }
    }
}
pub struct DoAttack;

impl <'a> System<'a> for DoAttack {
    type SystemData = (
        Entities<'a>,
        Read<'a, ::DeltaTime>,
        Read<'a, LazyUpdate>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, AttackTask>,
        WriteStorage<'a, sprite::Sprite>,
        WriteStorage<'a, Health>,
        ReadStorage<'a, Player>,
    );
    fn run(&mut self, (ents, delta, updater, mut pos, mut vel, mut task, mut spr, mut health, player): Self::SystemData) {
        let delta = delta.0;
        for (e, _, task, spr) in (&*ents, pos.mask().clone(), &mut task, &mut spr).join() {
            if vel.get(e).is_some() {
                updater.remove::<AttackTask>(e);
                continue;
            }
            if !ents.is_alive(task.target) {
                updater.remove::<AttackTask>(e);
                spr.set_state(sprite::SpriteState::Idle);
                continue;
            }
            let target_pos = pos.get(task.target).unwrap().clone();
            let pos = pos.get_mut(e).unwrap();

            if target_pos.x < pos.x {
                spr.facing = -1;
            } else {
                spr.facing = 1;
            }
            task.time -= delta;

            if task.time < -50.0 {
                // TODO: Blocking
                if spr.state == sprite::SpriteState::Attack {
                    health.get_mut(task.target).unwrap().health -= 10.0;
                }
                spr.set_state(sprite::SpriteState::Idle);
                updater.remove::<AttackTask>(e);
                let chance = if player.get(e).is_some() {
                    0.6
                } else {
                    0.1
                };
                if ::js::random() < chance {
                    vel.insert(task.target, Velocity {
                        x: (target_pos.x - pos.x).signum() * 1.5,
                        y: (target_pos.y - pos.y).signum() * 1.1,
                        z: 2.0,
                    });
                }
            } else if task.time <= 0.0 {
                spr.set_state(sprite::SpriteState::Attack);
            } else {
                pos.x += ((target_pos.x + task.offset - pos.x) / task.time as f32) * delta as f32;
                pos.y += ((target_pos.y - pos.y) / task.time as f32) * delta as f32;
                pos.z += ((target_pos.z - pos.z) / task.time as f32) * delta as f32;
                spr.set_state(sprite::SpriteState::Walk);
            }
        }
    }
}


pub struct DrawHealth;

impl <'a> System<'a> for DrawHealth {
    type SystemData = (
        Read<'a, Screen>,
        Read<'a, Context>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Health>,
        ReadStorage<'a, sprite::Sprite>,
    );
    fn run(&mut self, (screen, ctx, pos, health, spr): Self::SystemData) {
        for (pos, health, spr) in (&pos, &health, &spr).join() {
            ctx.save();
            ctx.translate(-screen.offset as i32 + pos.x as i32, 176 + (pos.y * 2.0) as i32 - pos.z as i32);

            let health_c = ((health.health / health.max_health) * 255.0) as u8;

            ctx.set_fill_style("#000000");
            ctx.fill_rect(
                -spr.info.offset.0, -spr.info.offset.1,
                spr.info.frame_size.0, 4,
            );
            ctx.set_fill_style(&format!("rgb({}, {}, 0)", 255 - health_c, health_c));
            ctx.fill_rect(
                -spr.info.offset.0, -spr.info.offset.1,
                (spr.info.frame_size.0 as f64 * (health.health / health.max_health)) as i32, 4,
            );

            ctx.restore();
        }
    }
}

pub struct DrawName;

impl <'a> System<'a> for DrawName {
    type SystemData = (
        Read<'a, Screen>,
        Read<'a, Context>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Named>,
        ReadStorage<'a, sprite::Sprite>,
    );
    fn run(&mut self, (screen, ctx, pos, name, spr): Self::SystemData) {
        ctx.set_text_align("center");
        for (pos, name, spr) in (&pos, &name, &spr).join() {
            ctx.save();
            ctx.translate(-screen.offset as i32 + pos.x as i32, 176 + (pos.y * 2.0) as i32 - pos.z as i32);

            ctx.set_fill_style("#FFFFFF");
            ctx.fill_text(&name.name, 0, -spr.info.offset.1 - 4);

            ctx.restore();
        }
        ctx.set_text_align("start");
    }
}
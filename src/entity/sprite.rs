
use specs::prelude::*;
use js::CanvasRenderingContext2D as Context;
use screen::Screen;
use itertools::Itertools;

pub const PLAYER: SpriteInfo = SpriteInfo {
    image: "player",
    frame_size: (48, 64),
    idle: (0, 3),
    walk: (4, 7),
    attack: (8, 11),
    offset: (24, 54),
};
pub const BAD1: SpriteInfo = SpriteInfo {
    image: "bad1",
    frame_size: (48, 64),
    idle: (0, 3),
    walk: (4, 7),
    attack: (8, 11),
    offset: (24, 54),
};

pub struct SpriteInfo {
    image: &'static str,
    pub frame_size: (i32, i32),
    idle: (i32, i32),
    walk: (i32, i32),
    attack: (i32, i32),
    pub offset: (i32, i32),
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Sprite {
    pub info: &'static SpriteInfo,
    pub state: SpriteState,
    frame_time: f64,
    pub facing: i32,
}

#[derive(PartialEq, Eq)]
pub enum SpriteState {
    Idle,
    Walk,
    Attack,
}

impl Sprite {
    pub fn new(info: &'static SpriteInfo) -> Sprite {
        Sprite {
            info: info,
            state: SpriteState::Idle,
            frame_time: 0.0,
            facing: 1,
        }
    }

    pub fn set_state(&mut self, state: SpriteState) {
        if state != self.state {
            self.state = state;
            self.frame_time = 0.0;
        }
    }
}

pub struct DrawSprites;

impl <'a> System<'a> for DrawSprites {
    type SystemData = (
        Read<'a, Screen>,
        Read<'a, Context>,
        Read<'a, ::ImageMap>,
        Read<'a, ::DeltaTime>,
        ReadStorage<'a, super::Position>,
        WriteStorage<'a, Sprite>,
    );
    fn run(&mut self, (screen, ctx, images, delta, pos, mut spr): Self::SystemData) {
        let delta = delta.0;
        for (pos, spr) in (&pos, &mut spr).join().sorted_by(|a, b| a.0.y.partial_cmp(&b.0.y).unwrap()) {
            ctx.save();
            ctx.translate(-screen.offset as i32 + pos.x as i32, 176 + (pos.y * 2.0) as i32 - pos.z as i32);
            ctx.scale(spr.facing as f64, 1.0);

            spr.frame_time += delta * 0.1;
            let (start, end) = match spr.state {
                SpriteState::Idle => spr.info.idle,
                SpriteState::Walk => spr.info.walk,
                SpriteState::Attack => spr.info.attack,
            };

            let len = end + 1 - start;
            while spr.frame_time > len as f64 {
                spr.frame_time -= len as f64;
            }

            let frame = start + spr.frame_time as i32;

            ctx.draw_image_part(
                images.get(spr.info.image).unwrap(),
                frame * spr.info.frame_size.0, 0, spr.info.frame_size.0, spr.info.frame_size.1,
                -spr.info.offset.0, -spr.info.offset.1,
                spr.info.frame_size.0, spr.info.frame_size.1
            );

            ctx.restore();
        }
    }
}


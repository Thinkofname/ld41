
use text_buffer::TextBuffer;
use js::CanvasRenderingContext2D as Context;
use itertools::Itertools;
use specs::prelude::*;
use entity::{self, Direction};

#[derive(Debug)]
pub enum CommandType {
    Take,
    Move,
    Attack,
}

#[derive(Debug)]
pub enum Command {
    Help(CommandType),
    Take {
        item: String,
    },
    Move {
        direction: Direction,
    },
    Attack {
        name: String,
    },
}

impl Command {
    pub fn do_command(self, ctx: &Context, world: &mut World, player: Entity) {
        let mut output = world.write_resource::<TextBuffer>();
        use self::Command::*;
        match self {
            Help(CommandType::Take) => {
                output.push_message(ctx, "Help: Tries to pick up the named #item.`");
                output.push_message(ctx, "Usage: 'take|pickup|grab|pick up' (item)");
            },
            Help(CommandType::Move) => {
                output.push_message(ctx, "Help: Tries to move in the named @direction.`");
                output.push_message(ctx, "Usage: 'move|walk|go' 'left|right|east|west|exit'");
            },
            Help(CommandType::Attack) => {
                output.push_message(ctx, "Help: Tries to attack in the named @target.`");
                output.push_message(ctx, "Usage: 'attack|hit|punch' (name)");
            },
            Take{item} => {
                output.push_message(ctx, &format!("You take the #{}.`", item));
                output.push_message(ctx, "Well you would have, if this was implemented.");
            },
            Move{direction} => {
                output.push_message(ctx, "You begin to move.");
                let pos = world.read::<entity::Position>();
                let pos = pos.get(player).unwrap();
                let mut task = world.write::<entity::MoveTask>();
                task.insert(player, entity::MoveTask {
                    x: pos.x + match direction {
                        Direction::Right => 125.0,
                        Direction::Left => -125.0,
                        _ => 0.0,
                    },
                    y: pos.y + match direction {
                        Direction::Up => -10.0,
                        Direction::Down => 10.0,
                        _ => 0.0,
                    },
                    z: 0.0,
                    time: (25.0 - pos.y).hypot(75.0) as f64,
                });
            },
            Attack{name} => {
                let pos = world.read::<entity::Position>();
                let named = world.read::<entity::Named>();
                let target = if let Some(target) = (&*world.entities(), &pos, &named).join()
                    .find(|v| v.2.name.to_lowercase().starts_with(&name))
                {
                    target
                } else {
                    output.push_message(ctx, "I can't hit that");
                    return
                };
                output.push_message(ctx, &format!("You attack {}", target.2.name));
                let pos = pos.get(player).unwrap();
                let mut task = world.write::<entity::AttackTask>();
                task.insert(player, entity::AttackTask {
                    target: target.0,
                    offset: if target.1.x < pos.x {
                        30.0
                    } else {
                        -30.0
                    },
                    time: (target.1.y - pos.y).hypot(target.1.x - pos.x) as f64,
                });
            },
        }
    }
}

pub fn parse_input(input: &str) -> Option<Command> {
    let mut input = input.split(' ');
    Some(match input.next()? {
        "take" | "pickup" | "grab" => {
            let item: String = input.join(" ");
            if item.is_empty() {
                return Some(Command::Help(CommandType::Take));
            }
            Command::Take {
                item: item,
            }
        },
        "pick" => if input.next()? == "up" {
            let item: String = input.join(" ");
            if item.is_empty() {
                return Some(Command::Help(CommandType::Take));
            }
            Command::Take {
                item: item,
            }
        } else { return None },
        "move" | "walk" | "go" => {
            match input.next() {
                Some("left") | Some("west") => Command::Move {
                    direction: Direction::Left,
                },
                Some("right") | Some("east") | Some("exit") => Command::Move {
                    direction: Direction::Right,
                },
                Some("up") | Some("north") => Command::Move {
                    direction: Direction::Up,
                },
                Some("down") | Some("south") => Command::Move {
                    direction: Direction::Down,
                },
                None => Command::Help(CommandType::Move),
                _ => return None,
            }
        },
        "attack" | "hit" | "punch" => {
            let name: String = input.join(" ");
            if name.is_empty() {
                return Some(Command::Help(CommandType::Attack));
            }
            Command::Attack {
                name: name,
            }
        },
        _ => return None,
    })
}

use wasm_bindgen::prelude::*;
use std::cell::RefCell;

#[wasm_bindgen]
extern {

    #[wasm_bindgen(js_name = requestAnimationFrame)]
    pub fn request_animation_frame(cb: &Closure<FnMut()>);

    pub type Performance;
    pub static performance: Performance;

    #[wasm_bindgen(method)]
    pub fn now(this: &Performance) -> f64;

    pub type HTMLDocument;
    pub static document: HTMLDocument;

    #[wasm_bindgen(method, js_name = createElement)]
    pub fn create_element_canvas(this: &HTMLDocument, tag_name: &str) -> HTMLCanvasElement;
    #[wasm_bindgen(method, js_name = createElement)]
    pub fn create_element_image(this: &HTMLDocument, tag_name: &str) -> HTMLImageElement;
    #[wasm_bindgen(method, js_name = getElementById)]
    pub fn get_element_by_id(this: &HTMLDocument, id: &str) -> HTMLElement;
    #[wasm_bindgen(method, getter)]
    pub fn body(this: &HTMLDocument) -> HTMLElement;
    #[wasm_bindgen(method, js_name = requestAnimationFrame, setter = onkeypress, structural)]
    pub fn on_key_press(this: &HTMLDocument, cb: &Closure<FnMut(KeyboardEvent)>);
    #[wasm_bindgen(method, js_name = requestAnimationFrame, setter = onkeydown, structural)]
    pub fn on_key_down(this: &HTMLDocument, cb: &Closure<FnMut(KeyboardEvent)>);

    pub type KeyboardEvent;
    #[wasm_bindgen(method, getter, js_name = key)]
    pub fn key(this: &KeyboardEvent) -> String;
    #[wasm_bindgen(method, js_name = preventDefault)]
    pub fn prevent_default(this: &KeyboardEvent);

    pub type HTMLElement;
    #[wasm_bindgen(method, js_name = appendChild)]
    pub fn append_child_canvas(this: &HTMLElement, other: &HTMLCanvasElement);
    #[wasm_bindgen(method, getter, js_name = clientWidth)]
    pub fn client_width(this: &HTMLElement) -> i32;
    #[wasm_bindgen(method, getter, js_name = clientHeight)]
    pub fn client_height(this: &HTMLElement) -> i32;
    #[wasm_bindgen(method, getter, js_name = style)]
    pub fn style(this: &HTMLElement) -> CSSStyleDeclaration;

    pub type CSSStyleDeclaration;
    #[wasm_bindgen(method, setter = display, js_name = display, structural)]
    pub fn set_display(this: &CSSStyleDeclaration, display: &str);

    pub type HTMLImageElement;
    #[wasm_bindgen(method, setter = src, structural)]
    pub fn set_source(this: &HTMLImageElement, src: &str);
    #[wasm_bindgen(method, getter = complete, structural)]
    pub fn complete(this: &HTMLImageElement) -> bool;
    #[wasm_bindgen(method, js_name = requestAnimationFrame, setter = onload, structural)]
    pub fn on_load(this: &HTMLImageElement, cb: &Closure<FnMut()>);

    pub type HTMLCanvasElement;
    #[wasm_bindgen(method, setter = width)]
    pub fn set_width(this: &HTMLCanvasElement, width: i32);
    #[wasm_bindgen(method, setter = height)]
    pub fn set_height(this: &HTMLCanvasElement, height: i32);
    #[wasm_bindgen(method, js_name = getContext)]
    pub fn get_context_2d(this: &HTMLCanvasElement, ty: &str) -> CanvasRenderingContext2D;

    pub type CanvasRenderingContext2D;

    #[wasm_bindgen(method, setter = fillStyle, structural)]
    pub fn set_fill_style(this: &CanvasRenderingContext2D, style: &str);
    #[wasm_bindgen(method, setter = font, structural)]
    pub fn set_font(this: &CanvasRenderingContext2D, font: &str);
    #[wasm_bindgen(method, setter = textAlign, structural)]
    pub fn set_text_align(this: &CanvasRenderingContext2D, align: &str);
    #[wasm_bindgen(method, setter = imageSmoothingEnabled, structural)]
    pub fn set_image_smoothing(this: &CanvasRenderingContext2D, enabled: bool);

    #[wasm_bindgen(method, js_name = fillRect)]
    pub fn fill_rect(this: &CanvasRenderingContext2D, x: i32, y: i32, w: i32, h: i32);
    #[wasm_bindgen(method, js_name = clearRect)]
    pub fn clear_rect(this: &CanvasRenderingContext2D, x: i32, y: i32, w: i32, h: i32);
    #[wasm_bindgen(method, js_name = fillText)]
    pub fn fill_text(this: &CanvasRenderingContext2D, text: &str, x: i32, y: i32);
    #[wasm_bindgen(method, js_name = save)]
    pub fn save(this: &CanvasRenderingContext2D);
    #[wasm_bindgen(method, js_name = restore)]
    pub fn restore(this: &CanvasRenderingContext2D);
    #[wasm_bindgen(method, js_name = translate)]
    pub fn translate(this: &CanvasRenderingContext2D, x: i32, y: i32);
    #[wasm_bindgen(method, js_name = scale)]
    pub fn scale(this: &CanvasRenderingContext2D, x: f64, y: f64);

    #[wasm_bindgen(method, js_name = drawImage)]
    pub fn draw_canvas_scaled(this: &CanvasRenderingContext2D, other: &HTMLCanvasElement, x: i32, y: i32, w: i32, h: i32);
    #[wasm_bindgen(method, js_name = drawImage)]
    pub fn draw_image_scaled(this: &CanvasRenderingContext2D, other: &HTMLImageElement, x: i32, y: i32, w: i32, h: i32);
    #[wasm_bindgen(method, js_name = drawImage)]
    pub fn draw_image_part(this: &CanvasRenderingContext2D, other: &HTMLImageElement, sx: i32, sy: i32, sw: i32, sh: i32, dx: i32, dy: i32, dw: i32, dh: i32);


    #[wasm_bindgen(method, js_name = measureText)]
    pub fn measure_text(this: &CanvasRenderingContext2D, text: &str) -> TextMetrics;
    pub type TextMetrics;

    #[wasm_bindgen(method, getter = width, structural)]
    pub fn width(this: &TextMetrics) -> i32;

    #[wasm_bindgen(js_namespace = console)]
    pub fn log(val: &str);

    #[wasm_bindgen(js_namespace = Math)]
    pub fn random() -> f64;

    pub type Location;
    pub static location: Location;
    #[wasm_bindgen(method, js_name = reload, structural)]
    pub fn reload(this: &Location);
}

thread_local! {
    static DRAW_FUNC: RefCell<Option<(Closure<FnMut()>, Box<FnMut()>)>> = RefCell::new(None);
}

pub fn set_drawer<F: FnMut() + 'static>(f: F) {
    DRAW_FUNC.with(move |df| {
        *df.borrow_mut() = Some((
            Closure::new(do_draw),
            Box::new(f)
        ));
    });
    do_draw();
}

fn do_draw() {
    DRAW_FUNC.with(move |df| {
        if let Some((ref c, ref mut func)) = *df.borrow_mut() {
            func();
            request_animation_frame(&c);
        }
    });
}
impl Default for CanvasRenderingContext2D {
    fn default() -> CanvasRenderingContext2D {
        unimplemented!()
    }
}
#!/bin/sh

set -ex

mkdir out || true
cargo +nightly build --target wasm32-unknown-unknown --release
wasm-bindgen target/wasm32-unknown-unknown/release/ld41.wasm --out-dir ./out/ --browser --no-modules
wasm-gc ./out/ld41_bg.wasm -o ./out/ld41_bg.opt.wasm
wasm-opt -O3 ./out/ld41_bg.opt.wasm -o ./out/ld41_bg.wasm
mv out/ld41_bg.wasm out/ld41.notwasm
npm run build
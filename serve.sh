#!/bin/sh

set -ex

mkdir out || true
cargo +nightly build --target wasm32-unknown-unknown
wasm-bindgen target/wasm32-unknown-unknown/debug/ld41.wasm --out-dir ./out/ --browser --no-modules
mv out/ld41_bg.wasm out/ld41.notwasm
npm run serve